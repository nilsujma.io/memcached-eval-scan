# Detailed Pipeline Overview for `.gitlab-ci.yml`

## Pipeline Stages

The pipeline is structured into six main stages: `security-gates`, `build`, `shiftleft-scan`, `azure-push`, `acr-shiftleft-scan`, and `deploy`.

### 1. Security Gates
Includes `spectral-secret-gate` and `spectral-iac-gate` stages, which perform comprehensive security scans on the codebase using SpectralOps. This ensures no secrets are exposed and IaC configurations are secure.

### 2. Build Image
In the `build_image` stage, a Docker image named `memcached-eval` is created. This stage involves building the image with Docker, tagging it, logging into the GitLab registry, and then pushing the image there.

### 3. ShiftLeft Security Scan
During the `shiftleft_scan` stage, the pipeline pulls the Docker image from the registry, saves it as `image.tar`, and then runs a ShiftLeft security scan on it. This step helps identify any potential vulnerabilities in the Docker image.

### 4. Push Image to Azure Container Registry (ACR)
The `push_to_acr` stage involves pulling the Docker image from the GitLab registry, logging into Azure ACR, tagging the image for ACR, and pushing it there. This stage ensures that the Docker image is available in Azure for further actions.

### 5. ACR ShiftLeft Scan
The `acr_shiftleft_scan` stage conducts an additional security scan of the Docker image now located in Azure Container Registry. The image is saved as `azureimage.tar`, and ShiftLeft performs another round of security scanning, ensuring the image’s security in the Azure environment.

### 6. Deploy to Azure Kubernetes Service (AKS)
Finally, in the `deploy_to_aks` stage, the pipeline deploys the Docker image to Azure Kubernetes Service. This is done by decoding the KUBECONFIG file, setting the necessary environment variables, and applying the Kubernetes deployment configuration using `kubectl`.

## Variables Used

- `IMAGE_NAME`: The name assigned to the Docker image, in this case, `memcached-eval`.
- `IMAGE_TAG`: A unique tag for the Docker image combining the GitLab registry path and the image name.
- `AZURE_IMAGE_TAG`: The tag used for the Docker image in Azure Container Registry.

## Key Points

- The pipeline primarily operates on the `main` branch.
- Security scans are a major focus at multiple stages, ensuring the Docker image's integrity throughout the process.
- Integration with Azure services is central to this pipeline, with both the Azure Container Registry and Azure Kubernetes Service being pivotal components.
- The use of Docker for container management, along with tools like Helm and kubectl for Kubernetes deployment, illustrates a modern, containerized application deployment approach.

### Best Practices

- Maintain regular updates to the `.gitlab-ci.yml` file to ensure it matches the current needs of your project.
- Keep credentials and sensitive information used in the pipeline secure and manage access appropriately.
- Regularly review the performance of the pipeline and adapt as necessary to improve efficiency and reliability.